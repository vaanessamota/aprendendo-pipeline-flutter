import 'package:aprendendo_pipeline_flutter/app/modules/home/home_controller.dart';
import 'package:aprendendo_pipeline_flutter/app/modules/home/home_module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:aprendendo_pipeline_flutter/app/modules/home/home_page.dart';

main() {

  initModule(HomeModule());

  HomeController home;
  
  setUp((){
    home = HomeModule.to.get<HomeController>();
  });

  testWidgets('HomePage has title', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(HomePage(title: 'Home')));
    final titleFinder = find.text('Home');
    expect(titleFinder, findsOneWidget);
  });

  testWidgets('Reverse String Widget Test', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(HomePage(title: 'Home')));
    var textField = find.byType(TextField);
    expect(textField, findsOneWidget);
    await tester.enterText(textField, 'String');
    expect(find.text('String'), findsOneWidget);
    var button = find.text('Reverter String');
    expect(button, findsOneWidget);
    await tester.tap(button);
    await tester.pump();
    expect(find.text('gnirtS'), findsOneWidget);
  });
}
