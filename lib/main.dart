import 'package:flutter/material.dart';
import 'package:aprendendo_pipeline_flutter/app/app_module.dart';
import 'package:flutter_modular/flutter_modular.dart';

void main() => runApp(ModularApp(module: AppModule()));
