// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeBase, Store {
  final _$valueAtom = Atom(name: '_HomeBase.value');

  @override
  int get value {
    _$valueAtom.reportRead();
    return super.value;
  }

  @override
  set value(int value) {
    _$valueAtom.reportWrite(value, super.value, () {
      super.value = value;
    });
  }

  final _$stringAtom = Atom(name: '_HomeBase.string');

  @override
  String get string {
    _$stringAtom.reportRead();
    return super.string;
  }

  @override
  set string(String value) {
    _$stringAtom.reportWrite(value, super.string, () {
      super.string = value;
    });
  }

  final _$modifiedStringAtom = Atom(name: '_HomeBase.modifiedString');

  @override
  String get modifiedString {
    _$modifiedStringAtom.reportRead();
    return super.modifiedString;
  }

  @override
  set modifiedString(String value) {
    _$modifiedStringAtom.reportWrite(value, super.modifiedString, () {
      super.modifiedString = value;
    });
  }

  final _$_HomeBaseActionController = ActionController(name: '_HomeBase');

  @override
  void increment() {
    final _$actionInfo =
        _$_HomeBaseActionController.startAction(name: '_HomeBase.increment');
    try {
      return super.increment();
    } finally {
      _$_HomeBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeString(dynamic value) {
    final _$actionInfo =
        _$_HomeBaseActionController.startAction(name: '_HomeBase.changeString');
    try {
      return super.changeString(value);
    } finally {
      _$_HomeBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic reverseString() {
    final _$actionInfo = _$_HomeBaseActionController.startAction(
        name: '_HomeBase.reverseString');
    try {
      return super.reverseString();
    } finally {
      _$_HomeBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
value: ${value},
string: ${string},
modifiedString: ${modifiedString}
    ''';
  }
}
