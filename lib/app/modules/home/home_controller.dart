import 'package:mobx/mobx.dart';

part 'home_controller.g.dart';

class HomeController = _HomeBase with _$HomeController;

abstract class _HomeBase with Store {
  

  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
  
  @observable
  String string = "";

  @action
  changeString(value) => string = value;

  @observable
  String modifiedString = "";

  @action
  reverseString() {
    modifiedString = string.split('').reversed.join();

  }

}
